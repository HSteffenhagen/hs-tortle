module MonadDSL
( DSLM
, fromAction
, interpret
) where

data DSLM f a where
  DSLPure :: a -> DSLM f a
  DSLCont :: f a -> (a -> DSLM f b) -> DSLM f b

instance Functor (DSLM f) where
  fmap f (DSLPure x) = DSLPure $ f x
  fmap f (DSLCont a cont) = DSLCont a (fmap f . cont)

instance Applicative (DSLM f) where
  pure = DSLPure
  DSLPure f <*> m = fmap f m
  DSLCont a f_cont <*> m = DSLCont a ((<*> m) . f_cont)

instance Monad (DSLM f) where
  DSLPure x >>= f = f x
  DSLCont a x_cont >>= f = DSLCont a ((>>= f) . x_cont)

fromAction :: f a -> DSLM f a
fromAction a = DSLCont a DSLPure

interpret :: Monad m => (forall a. f a -> m a) -> DSLM f b -> m b
interpret _ (DSLPure x) = return x
interpret eval (DSLCont a cont) = eval a >>= (interpret eval . cont)