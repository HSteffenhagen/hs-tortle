module Tortle
( Position (..)
, Tortle (..)
, TortleM
, moveTo
, getPosition
, lineTo
) where

import MonadDSL

data Position = Position !Double !Double
  deriving Show

data Tortle a where
  MoveTo :: Position -> Tortle ()
  GetPosition :: Tortle Position
  LineTo :: Position -> Tortle ()

deriving instance Show a => Show (Tortle a)

type TortleM = DSLM Tortle

moveTo :: Position -> TortleM ()
moveTo p = fromAction $ MoveTo p

getPosition :: TortleM Position
getPosition = fromAction $ GetPosition

lineTo :: Position -> TortleM ()
lineTo p = fromAction $ LineTo p