module Main where

import Tortle
import MonadDSL(interpret)

import Control.Monad.State
import Control.Monad.Writer
import Data.Foldable

data TortleState = TortleState { tortlePosition :: Position }

initialTortleState :: TortleState
initialTortleState = TortleState { tortlePosition = Position 0 0}

type TortleTrace = [String]

type TortleTraceM = StateT TortleState (Writer TortleTrace)

stringifyTortle :: Tortle a -> String
stringifyTortle a = case a of
  MoveTo _ -> show a
  GetPosition -> show a
  LineTo _ -> show a

tortleTraceEvaluate :: Tortle a -> TortleTraceM a
tortleTraceEvaluate a = do
  tell [stringifyTortle a]
  case a of
    MoveTo p -> modify $ \s -> s {tortlePosition = p}
    GetPosition -> gets tortlePosition
    LineTo p -> modify $ \s -> s {tortlePosition = p}

getTortleTrace :: TortleM a -> [String]
getTortleTrace
  = execWriter
  . flip evalStateT initialTortleState
  . interpret tortleTraceEvaluate

program :: TortleM ()
program = do
  moveTo $ Position 10 15
  Position x y <- getPosition
  lineTo $ Position y x

main :: IO ()
main = traverse_ putStrLn $ getTortleTrace program
